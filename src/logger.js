var chalk = require('chalk');

var themes = {
  title: chalk.blue.bold.underline,
  normal: chalk.white,
  info: chalk.cyan,
  error: chalk.red,
  success: chalk.green,
  href: chalk.blue.underline
};

function log (label, value, theme) {
  theme = theme || themes.info;
  console.log(theme(label + ':'), value);
}

module.exports.title = function (title, ref) {
  if (ref) {
    console.log(themes.title.cyan('[' + ref + ']'), themes.title(title));
    return;
  }
  console.log(themes.title(title));
};

module.exports.info = function (label, value) {
  log(label, value, themes.info);
};

module.exports.success = function (message) {
  log('Success', message, themes.success);
};

module.exports.error = function (err) {
  log('Error', err.message, themes.error);
};

module.exports.href = function (href) {
  console.log(themes.href(href));
};