var request = require('./request');

module.exports = {
  
  get: function (owner, slug, opts) {
    opts = opts || {};
    
    var url = '/2.0/repositories';
    if (owner) {
      url = url + '/' + owner;
      
      if (slug) {
        url = url + '/' + slug;
      }
    }
    
    return request({
      uri: url,
      qs: {
        page: opts.page,
        pagelen: opts.limit
      }
    });
  },
  
  create: function (owner, slug, opts) {
    opts = opts || {};
        
    return request({
      method: 'POST',
      uri: '/2.0/repositories/' + owner + '/' + slug,
      body: {
        name: opts.name,
        description: opts.description,
        is_private: !!opts.private,
        scm: opts.scm
      }
    });
  }
  
};