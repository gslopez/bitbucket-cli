var request = require('./request');

module.exports = {
  
  get: function (teamName) {
    return request({
      uri: '/2.0/teams/' + teamName
    });
  },
  
  members: function (teamName, opts) {
    return request({
      uri: '/2.0/teams/' + teamName + '/members',
      qs: {
        page: opts.page,
        pagelen: opts.limit
      }
    });
  }
  
};