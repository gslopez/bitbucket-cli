var Promise = require('bluebird');

var request = Promise.promisify(require('request').defaults({
  baseUrl: 'https://bitbucket.org/api/',
  oauth: {
    consumer_key: process.env.BITBUCKET_KEY,
    consumer_secret: process.env.BITBUCKET_SECRET
  },
  json: true
}));

module.exports = function (opts) {
  return request(opts).spread(function (res, body) {
    if (res.statusCode !== 200) {
      if (body && body.error) throw Error(body.error.message);
      throw Error('could not complete request');
    }
    
    return Promise.resolve(body);
  });
};

