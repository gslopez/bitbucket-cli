var request = require('./request');

module.exports = {
  
  get: function (username) {
    return request({
      uri: '/2.0/users/' + username
    });
  }
  
};