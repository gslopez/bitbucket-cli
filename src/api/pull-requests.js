var request = require('./request');

module.exports = {
  
  get: function (owner, slug, id, opts) {
    opts = opts || {};
    
    var url = '/2.0/repositories/' + owner + '/' + slug + '/pullrequests';
  
    if (id) {
      url = url + '/' + id;
    }
    
    return request({
      uri: url,
      qs: {
        page: opts.page,
        pagelen: opts.limit
      }
    });
  }
  
};