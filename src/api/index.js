module.exports = {
  
  users: require('./users'),
  teams: require('./teams'),
  repositories: require('./repositories'),
  pullRequests: require('./pull-requests')
  
};