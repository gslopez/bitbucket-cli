var program = require('commander');

var api = require('../api');
var logger = require('../logger');

function getUser (username, opts) {
  api.users.get(username)
    .then(function (body) {
    logger.info('Username', body.username);
    logger.info('Name', body.display_name);

    if (body.location) {
      logger.info('Location', body.location);      
    }

    if (body.website) {
      logger.info('Website', body.website);      
    }
    
    if (opts.browse) {
      logger.href(body.links.html.href);
    }
  }).catch(logger.error);
}

program
  .command('user <username>')
  .description('')
  .option('-b, --browse', 'Show href to browse resource')
  .action(getUser);