var program = require('commander');

var api = require('../api');
var logger = require('../logger');

function getTeamMembers (teamName, opts) {
  api.teams.members(teamName, opts)
    .then(function (body) {
    body.values.forEach(function (member) {
      logger.info(member.username, member.display_name);
    });
  }).catch(logger.error);
}

program
  .command('team-members <teamName>')
  .description('')
  .option('-p, --page <page>', 'Set page number', 1)
  .option('-l, --limit <limit>', 'Set page limit', 10)
  .action(getTeamMembers);