var program = require('commander');

var api = require('../api');
var logger = require('../logger');

function getTeam (teamName, opts) {
  api.teams.get(teamName)
    .then(function (body) {
    logger.info('Username', body.username);
    logger.info('Name', body.display_name);
    
    if (opts.browse) {
      logger.href(body.links.html.href);
    }
  }).catch(logger.error);
}

program
  .command('team <teamName>')
  .description('')
  .option('-b, --browse', 'Show href to browse resource')
  .action(getTeam);