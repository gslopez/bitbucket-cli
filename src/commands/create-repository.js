var program = require('commander');

var api = require('../api');
var logger = require('../logger');

var createRepository = function (owner, slug, opts) {
  api.repositories.create(owner, slug, opts)
    .then(function (body) {
    logger.success('Repository successfully created');
  }).catch(logger.error);
};

program
  .command('create-repository <owner> <slug>')
  .alias('create-repo')
  .description('')
  .option('-n, --name <name>', 'Set repository name')
  .option('-d, --description <description>', 'Set repository description')
  .option('-p, --private', 'Make the repo private')
  .option('-s, --scm <scm>', 'Set repository SCM', 'git')
  .action(createRepository);