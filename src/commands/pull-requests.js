var program = require('commander');
var moment = require('moment');

var api = require('../api');
var logger = require('../logger');

function outputPullRequest (pullRequest, showDetail, opts) {
  logger.title(pullRequest.title, pullRequest.id);
  logger.info('Author', pullRequest.author.username);
  
  if (showDetail) {
    var created = moment(pullRequest.created_on);
    logger.info('Created', created.format('DD/MM/YYYY HH:mm'));

    var updated = moment(pullRequest.updated_on);
    logger.info('Updated', updated.format('DD/MM/YYYY HH:mm'));
  }
  
  logger.info('Source Branch', pullRequest.source.branch.name);
  logger.info('Destination Branch', pullRequest.destination.branch.name);
  
  if (showDetail) {
    logger.info('Close Source', pullRequest.close_source_branch);
  }
  
  logger.info('State', pullRequest.state);
  
  if (showDetail && opts.browse) {
    logger.href(pullRequest.links.html.href);
  }
}

var getPullRequests = function (owner, slug, id, opts) {
  api.pullRequests.get(owner, slug, id, opts)
    .then(function (body) {
    if (Array.isArray(body.values)) {
      body.values.forEach(function (pr) {
        outputPullRequest(pr, false, opts);
      });      
      return;
    }
    outputPullRequest(body, true, opts);
  }).catch(logger.error);
};

program
  .command('pull-requests <owner> <slug> [id]')
  .description('')
  .option('-p, --page <page>', 'Set page number', 1)
  .option('-l, --limit <limit>', 'Set page limit', 10)
  .option('-b, --browse', 'Show href to browse resource')
  .action(getPullRequests);