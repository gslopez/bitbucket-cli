var program = require('commander');
var moment = require('moment');

var api = require('../api');
var logger = require('../logger');

function outputRepo (repo, showDetail, opts) {
  logger.title(repo.full_name);

  logger.info('Scm', repo.scm);
  logger.info('Private', repo.is_private);

  if (showDetail) {
    var created = moment(repo.created_on);
    logger.info('Created', created.format('DD/MM/YYYY HH:mm'));

    var updated = moment(repo.updated_on);
    logger.info('Updated', updated.format('DD/MM/YYYY HH:mm'));
  }
  
  if (repo.language) {
    logger.info('Language', repo.language);        
  }
  
  if (showDetail && opts.browse) {
    logger.href(repo.links.html.href);
  }
}

var getRepositories = function (owner, slug, opts) {
  api.repositories.get(owner, slug, opts)
    .then(function (body) {
    if (Array.isArray(body.values)) {
      body.values.forEach(function (repo) {
        outputRepo(repo, false, opts);
      });      
      return;
    }
    outputRepo(body, true, opts);
  }).catch(logger.error);
};

program
  .command('repositories [owner] [slug]')
  .alias('repos')
  .description('')
  .option('-p, --page <page>', 'Set page number', 1)
  .option('-l, --limit <limit>', 'Set page limit', 10)
  .option('-b, --browse', 'Show href to browse resource')
  .action(getRepositories);