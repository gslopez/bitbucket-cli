var program = require('commander');
var package = require('../package');

program
  .version(package.version)
  .description(package.description)
  .action(function () {
    program.outputHelp();
  });

var commands = require('path').join(__dirname, 'commands');
require('fs').readdirSync(commands).forEach(function(file) {
  require('./commands/' + file);
});

program.parse(process.argv);

if (!process.argv.slice(2).length) {
  program.outputHelp();
}