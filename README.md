# Bitbucket CLI
Interface with Bitbucket via their [RESTful api](https://api.bitbucket.org) with the command line tool.

> **Note:** This tool is not affliated with Bitbucket or Atlassian. It is a third party library that simply interfaces with Bitbucket's API.

## Getting Started
The Bitbucket CLI is installed and managed via [npm](https://npmjs.org/), the [Node.js](http://nodejs.org/) package manager.

Before setting up ensure that your [npm](https://npmjs.org/) is up-to-date by running ```npm update -g npm``` (this might require sudo on certain systems).

### Installing 
In order to get started, you'll want to install the command line interface (CLI) globally. You may need to use sudo (for OSX, \*nix, BSD etc) or run your command shell as Administrator (for Windows) to do this.

```
npm install -g bitbucket-cli
```
This will put the ```bitbucket``` command in your system path, allowing it to be run from any directory.

To confirm it has installed correctly run ```bitbucket --version```.

### Obtaining an OAuth Key
The CLI uses OAuth when communicating with Bitbucket. Before using the CLI you need to obtain a consumer key and secret.

Bitbucket explain how to do this [here](https://confluence.atlassian.com/display/BITBUCKET/OAuth+on+Bitbucket#OAuthonBitbucket-Step1.CreateanOAuthkeyandsecret).

### Using the CLI
Once the CLI is installed you're good go. Get all the available commands with  ```bitbucket --help```.

There are two ways of giving the CLI you consumer settings.

```
export BITBUCKET_KEY="MY_CONSUMER_KEY"
export BITBUCKET_SECRET="MY_CONSUMER_SECRET"
bitbucket ...
```

```
BITBUCKET_KEY="MY_CONSUMER_KEY" BITBUCKET_SECRET="MY_CONSUMER_SECRET" bitbucket ...
```

## License
Copyright (c) 2015 Liam Moat

Released under [the MIT license](https://bitbucket.org/liammoat/bitbucket-cli/raw/master/LICENCE).